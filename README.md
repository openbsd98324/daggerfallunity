# daggerfallunity

## Introduction

Running on linux ...

![](1624724009/screenshot-1624724819.png)




## Installation  

Download: https://gitlab.com/openbsd98324/daggerfallunity/-/blob/main/1624724009/DaggerFall-Unity-DF2.tar.gz.dat

Copy the game to /opt 
and give the permissions.





## Drivers for graphic card on ArchLinux

Archlinux is quite adapted, but it depends it is not necessarily working all the time on all cards.

Linux OS like Archlinux with good drivers for Ryzen


Untar the rootfs and use grub2 to boot:

 https://gitlab.com/openbsd98324/linux-archlinux-20210601/-/raw/main/v1/ARCHLINUX.tar.gz



```
menuentry 'Linux Archlinux AMD64 on sda4 (5.12, Gaming)' --class devuan --class gnu-linux --class gnu { 
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos4'
        linux   /boot/vmlinuz-linux root=/dev/sda4  rw  
        initrd  /boot/initramfs-linux.img
}
```

2021, likely a new bug: 
Fails to work now on Archlinux,
Unix Time: 1632112700 


## Running on OpenSuse

Tested with /opt/daggerfallunity-main/1624724009/DaggerFall-Unity-DF2/DaggerfallUnity.x86_64   

It works on second attempt on opensuse.


 The rootfs of opensuse is preinstalled and available at url: 
 
 https://gitlab.com/openbsd98324/opensuse-leap-15.3/-/raw/main/rootfs/opensuse-leap-15.3-amd64-x86_64-preinstalled-KDE-notebook.tar.gz

![](1631967301-v02-opensuse/opensuse-daggerfall-unity.png)


Kernel: 
uname -a

Linux localhost.localdomain 5.3.18-57-default #1 SMP Wed Apr 28 10:54:41 UTC 2021 (ba3c2e9) x86_64 x86_64 x86_64 GNU/Linux






Select to advanced and not to start into the dungeon. 
You will start into the land.

Press 'v' for map and select to travel





## Exploring the game and cheats

Advanced and select TAB to CONSOLE.

Type e.g. into console: 

levitate on


cm 30 1 

cm 5  2

cm 6 3

add weapon 5

 


